import csv
import json
import math
import signal
import socket
import sys
import time
import numpy as np


class PID:
    def __init__(self, kp, ki, kd):
        self.e_old = 0.0
        self.E = 0.0
        self.kp = kp
        self.ki = ki
        self.kd = kd

    def pid(self, e):
        e_dot = e - self.e_old
        self.e_old = e
        self.E = self.E + e
        u = self.kp * e + self.ki * self.E + self.kd * e_dot
        return u


class Track():
    def __init__(self, json):
        self.pieces = json['pieces']
        self.lanes = dict([(l['index'], l) for l in json['lanes']])

    def piece_length(self, piece_inx, lane_inx):
        piece = self.pieces[piece_inx]

        if 'length' in piece:
            return piece['length']
        else:
            radius = self.piece_radius(piece_inx, lane_inx)

            return abs(piece['angle']) * (2.0 * np.pi / 360.0) * radius

    def piece_radius(self, piece_inx, lane_inx):
        piece = self.pieces[piece_inx]
        lane = self.lanes[lane_inx]
        if 'angle' in piece:
            if piece['angle'] > 0:
                radius = piece['radius'] - lane['distanceFromCenter']
            else:
                radius = piece['radius'] + lane['distanceFromCenter']
        else:
            radius = np.inf
        return radius


class Car():
    def __init__(self):
        self.lane = None
        self.current_piece = 0
        self.last_piece = 0
        self.current_pos = 0.0
        self.last_pos = 0.0
        self.current_speed = 0.0
        self.last_speed = 0.0
        self.acceleration = 0.0
        self.angle = 0.0
        self.max_drift = 0.0

    def update_status(self, track, pos):
        self.last_pos = self.current_pos

        self.current_pos = pos['piecePosition']['inPieceDistance']

        self.last_piece = self.current_piece
        self.current_piece = pos['piecePosition']['pieceIndex']

        self.lane = pos['piecePosition']['lane']['startLaneIndex']

        if self.current_piece != self.last_piece:
            start = track.piece_length(self.last_piece, self.lane) - self.last_pos
            delta_s = start + self.current_pos
            self.max_drift = abs(self.angle)
        else:
            delta_s = self.current_pos - self.last_pos
            if abs(self.angle) > self.max_drift:
                self.max_drift = abs(self.angle)

        delta_t = 1  # assume 1 tick
        self.last_speed = self.current_speed
        self.current_speed = delta_s / delta_t

        delta_v = self.current_speed - self.last_speed
        self.acceleration = delta_v / delta_t

        self.angle = pos['angle']


class NoobBot(object):
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.our_color = None
        self.game_init = None
        self.track = None
        self.pid = PID(16, 0, 1)
        self.turbo = None

        self.current_tick = 0
        self.t = 1

        # keep all cars status
        self.cars = dict()

        # record keeping
        self.states = []

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def join_race(self, track_name, car_count):
        return self.msg("joinRace", {"botId": {"name": self.name, "key": self.key},
                                     "trackName": track_name,
                                     #"password": "nothing",
                                     "carCount": car_count})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def use_turbo(self):
        self.msg("turbo", "Vruuuuuum!")
        self.turbo = None

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_your_car(self, data):
        self.our_color = data['color']
        print("Our color is {0}".format(self.our_color))
        self.ping()

    def on_game_init(self, data):
        self.game_init = data
        self.track = Track(self.game_init['race']['track'])

        print("Game: {0} pieces, {1} lanes, {2} cars".format(
            len(self.game_init['race']['track']['pieces']),
            len(self.game_init['race']['track']['lanes']),
            len(self.game_init['race']['cars'])))

        for i in self.game_init['race']['track']['pieces']:
            print(i)

        for car in data['race']['cars']:
            self.cars[car['id']['color']] = Car()

        self.ping()

    def on_car_positions(self, data):

        for car_pos in data:
            self.cars[car_pos['id']['color']].update_status(self.track, car_pos)

        our_car = self.cars[self.our_color]

        straight = 10
        d = straight

        p = our_car.current_piece
        np = (p + 1) % len(self.track.pieces)

        if 'radius' in self.track.pieces[p]:
            if our_car.angle < our_car.max_drift and not('radius' in self.track.pieces[np]):
                d = straight
            else:
                r = self.track.piece_radius(p, our_car.lane)
                d = math.sqrt(r) / 1.55
                #d = 3
        elif 'radius' in self.track.pieces[np]:
            r = self.track.piece_radius(np, our_car.lane)
            d = math.sqrt(r) / 1.25
            #d = 3
        #magic numbers
        #keimola: 1.5 and 1.2

        e = d - our_car.current_speed

        u = self.pid.pid(e)
        self.t = min(max(u, 0), 1)

        self.throttle(self.t)

        self.log_state()

        print('s: {0} a:{1} phi:{2} t:{3} u:{4} d:{5}'.format(
            our_car.current_speed, our_car.acceleration, our_car.angle,
            self.t, u, d))

    def on_crash(self, data):
        print("Someone crashed")
        color = data['color']
        print("{0} crashed".format(color))
        print(self.cars[color].acceleration)
        print(self.cars[color].current_speed)
        self.ping()

    def on_turbo_available(self, data):
        print('Turbo available: {0} {1} {2}'.format(
            data['turboDurationMilliseconds'],
            data['turboDurationTicks'],
            data['turboFactor']))
        self.turbo = data
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.dump_states()
        self.ping()

    def lap_finished(self, data):
        print('lap finished')

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'turboAvailable': self.on_turbo_available,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'lapFinished': self.lap_finished,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if 'gameTick' in msg:
                self.current_tick = msg['gameTick']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

    def log_state(self):
        our_car = self.cars[self.our_color]
        self.states.append({
            'tick': self.current_tick,
            'speed': our_car.current_speed,
            'accel': our_car.acceleration,
            'angle': our_car.angle,
            'piece': our_car.current_piece,
            'lane': our_car.lane,
            'radius': self.track.piece_radius(our_car.current_piece, our_car.lane),
            't': self.t,
        })

    def dump_states(self):
        if len(self.states) > 0:
            # render filename
            ts = time.strftime("%Y%m%d_%H%M%S")
            track_id = self.game_init['race']['track']['id']
            filename = 'logs/{0}_{1}.csv'.format(track_id, ts)

            with open(filename, 'wb') as f:
                keys = self.states[0].keys()
                dict_writer = csv.DictWriter(f, keys)
                dict_writer.writer.writerow(keys)
                dict_writer.writerows(self.states)


def signal_handler(signal, frame):
    bot.dump_states()
    sys.exit(0)

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        # setup signal listener
        signal.signal(signal.SIGINT, signal_handler)

        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
